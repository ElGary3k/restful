# RESTful


## Descripcion
- El proyecto consiste en una API REST utilizando Node.js y Express que permita realizar operaciones matemáticas básicas a través de solicitudes HTTP. La API realiza lo siguiente:

-Suma (GET)
-Multiplicación (POST)
-División (PUT)
-Potencia (PATCH)
-Resta (DELETE)

## Intrucciones: 
- Cuando se descarga el proyecto se debe acceder a la carpeta descargada y ejecutar el comando "node app.js" para encender el servidor
- En Postman, se agrega la url "http://localhost:3000/results/", luego entrar a body y en raw con JSON escribir, por ejemplo:
"{
"n1": 5,
"n2": 3
}"
-Luego se selecciona alguno de los metodos y enviar



## Requisitos
- node.js
- Postman

## Autor

- Angel Eduardo Garibay Valenzuela
- matricula:348775